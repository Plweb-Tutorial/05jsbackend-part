const PORT = process.env.PORT || 3000;

var express = require("express");
var path = require("path");
var app = express();
var bodyParser = require("body-parser");

var sqlite3 = require("sqlite3");
var db = new sqlite3.Database(__dirname + "/cart");

var indexRoute = require("./routes/index");

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, 'views'));
app.use(express.static(__dirname + "/public"));

db.serialize(function() {
  db.run("CREATE TABLE IF NOT EXISTS ______ (" + 
          "username TEXT PRIMARY KEY, " + 
          "______ TEXT, " +
          "______ ______)");
});

app.use("/", indexRoute);

app.listen(PORT, function(){
  console.log("Starting the server port " + PORT);
})